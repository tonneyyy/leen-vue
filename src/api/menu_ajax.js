//引用axios库
import request from '@/utils/request'
//调用方法
import {getServerIP} from '@/utils/auth'


//可以被别人调用（导出）
//请求java后台部门数据列表

export function getTreeData(){
  return  request({
    method:'get',
    url:getServerIP()+'/leen/menu/data'
  })
}

export function saveData(data){
  let url = '/leen/menu/add';
  if(data.menuId){
    url = '/leen/menu/update';
  }
  console.log(data);
  return  request({
    method:'post',
    url:getServerIP()+url,
    data
  })
}
