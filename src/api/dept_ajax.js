// 导入（引用） axios ajax库
// import  对象名  from   模块.js
import request from '@/utils/request'
// 导入服务URL地址 方法
import { getServerIP } from '@/utils/auth'

// 可以被别人调用 (导出)
// 请求java后台部门数据列表
export function getData() {
  // 把request()  看作  $.ajax();
  return  request({
    method: 'get',
    url: getServerIP()+ '/car/dept/data'
  })
}
