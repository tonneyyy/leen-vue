import request from '@/utils/request'
import { getServerIP } from '@/utils/auth'

/**
 * @param {Object} params 是一个javascript对象 { pageNumber: 1,  pageSize: 5   }
 */
export function getData(params) {
  return  request({
    method: 'get',
    url: getServerIP()+ '/leen/role/data',
    params: params
  })
}

// 根据角色id 查询拥有菜单权限编号  [1,2,3,4]
export function getOwnMenuId(roleId){
  // leen/role/1/ownmenus
  return  request({
    method: 'get',
    url: getServerIP()+ `/leen/role/${roleId}/ownmenus`
  })
}

// post请求
export function saveData(data) {  
  let serverUrl='/leen/role/add'  
  if(data.roleId) {
    serverUrl=  '/leen/role/update'
  }
  
  return request({
    method:'post',
    url: getServerIP() + serverUrl,
    data: data
  })
}