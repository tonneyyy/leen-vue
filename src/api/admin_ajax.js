//引用axios库
import request from '@/utils/request'
//调用方法
import {getServerIP} from '@/utils/auth'


//可以被别人调用（导出）
//请求java后台部门数据列表
export function getData(params){
  return  request({
    method:'get',
    url:getServerIP()+'/leen/admin/data',
    params
  })
}


// post请求
export function saveData(data) {
  let serverUrl='/leen/admin/add'
  if(data.id) {
    serverUrl=  '/leen/admin/update'
  }
  return request({
    method:'post',
    url: getServerIP() + serverUrl,
    data: data
  })
}

export function getRoleData(){
  return  request({
    method:'get',
    url:getServerIP()+'/leen/admin/roleData'
  })
}
