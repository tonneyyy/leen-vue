//引用axios库
import request from '@/utils/request'
//调用方法
import {getServerIP} from '@/utils/auth'


//可以被别人调用（导出）
//请求java后台部门数据列表

export function getData(){
  return  request({
    method:'get',
    url:getServerIP()+'/leen/courseCategory/data'
  })
}
export function saveData(data){
  let url = '/leen/courseCategory/add';
  if(data.id){
    url = '/leen/courseCategory/update';
  }
  console.log(data);
  return  request({
    method:'post',
    url:getServerIP()+url,
    data
  })
}
