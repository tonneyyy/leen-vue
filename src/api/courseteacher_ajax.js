//引用axios库
import request from '@/utils/request'
//调用方法
import {
  getServerIP
} from '@/utils/auth'


//可以被别人调用（导出）
//请求java后台课程老师数据列表
export function getData() {
  return request({
    method: 'get',
    url: getServerIP() + '/leen/courseteacher/data'
  })
}

// post请求
export function saveData(data) {
  let serverUrl = '/leen/courseteacher/add'
  if (data.id) {
    serverUrl = '/leen/courseteacher/update'
  }

  return request({
    method: 'post',
    url: getServerIP() + serverUrl,
    data: data
  })
}

export function deleteData(id) {
  return request({
    method: 'get',
    url: getServerIP() + '/leen/courseteacher/delete?id='+id,
  })
}
