import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

// 服务器的IP地址
export function getServerIP() {
  //return 'http://192.168.25.91:28080'
  return 'http://127.0.0.1:28080'
}
