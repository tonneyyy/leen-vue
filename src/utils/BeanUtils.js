
/** 复制对象的值
 * @param {Object} source  原对象 (原来对象)
 * @param {Object} target  新对象 (目标对象)
 */
export function copyProperties(source, target) {
  
  // 循环 新对象，key 键 
  for( let  key  in target){
    // 判断 原来对象 是否拥有 key 
    if( source.hasOwnProperty(key)) {
      // 赋值
      target[key] = source[key]
    }
  } 
}