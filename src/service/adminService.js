// 引用数据
import {getData,getRoleData,saveData} from '@/api/admin_ajax'

let vueData;

function init(_vueData){
  vueData = _vueData;
}

//私有方法，用于加载部门数据
function loadData(){
  getData(vueData.searchFrom).then(resp =>{
    if(resp.code === 1){
      vueData.responseVO = resp.data
    }
  })
  getRoleData().then(resp =>{
    vueData.roleData = resp.data;
  });
}

function editForm(data){
  vueData.title = '修改用户';
  vueData.dialogFormVisible = true;
  //还原数据
  let obj = Object.assign({},vueData.form,data);
  vueData.form = obj;
}

function addForm(){
  vueData.title = '新增用户';
  vueData.dialogFormVisible = true;
  //清空数据
  vueData.form = {
          loginName: undefined,
          mobile: undefined,
          sex: 0,
          status: 0,
          roleIds: []
        }
}


function saveForm(){
  //验证数据有效性
  let valid = true;
  if(valid){
    saveData(vueData.form).then((resp)=>{
      //关闭对话框
      vueData.dialogFormVisible = false
      //重新加载数据
      loadData()
    })
  }
}

//打包，变为一个模块(类)
export default {
  init,
  loadData,
  addForm,
  editForm,
  saveForm
}
