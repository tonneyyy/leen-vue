// 引用 数据访问层
import { getData } from '@/api/dept_ajax'


let vueData;

function init(_vueData){
  vueData=_vueData;
}

// 私有的方法(只能在deptService.js文件中使用)，用于加载 部门数据
function loadData() {
   //访问ajax
   getData().then( resp => {
     vueData.treeData = resp.data
   })
   //等价于
   // getData().then( function(resp) {

   // })
}


// 打包，变为一个类(模块)
export default {
  init,
  loadData
}
