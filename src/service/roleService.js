// 引用 数据访问层
import { getData , getOwnMenuId ,saveData} from '@/api/role_ajax'
import { getTreeData } from '@/api/menu_ajax'
import { copyProperties } from '@/utils/BeanUtils'

let vueData;

function init(_vueData){
  vueData=_vueData;
  // 加载树型菜单数据
  loadMenuTreeData()
}

//加载树型菜单数据
function loadMenuTreeData() {
  getTreeData().then( resp => {
    vueData.menuTreeData = resp.data
  })
}

// 加载数据
function loadData(){
  getData(vueData.searchForm).then( resp => {
    vueData.responseVO = resp.data
  })
}

function editForm (data) {
  vueData.title= "修改角色"
  // 显示对话框
  vueData.dialogFormVisible = true

  //树型节点互子不关联
  vueData.checkStrictly=true

  // 还原数据
  console.log(data);
  let row =  data

//在下次 DOM 更新循环结束之后执行延迟回调
    vueData.$nextTick(() => {
      // 重置表单内容
      vueData.$refs.form.resetFields()

      copyProperties(row, vueData.form)

      // 要这样使用 <el-tree   node-key="menuId 主键"  >
      // 根据角色id 动态获取 权限
      getOwnMenuId(row.roleId).then( resp => {
        vueData.$refs.menuTree.setCheckedKeys( resp.data)

        //再把树型关联
        //树型节点互子不关联
        vueData.checkStrictly=false

      })

    })
}

// 保存操作
function saveForm() {
  // 验证数据有效性
  let valid =true;
  if(valid) {
    //数据保存 ,取得当前选中的数据
    let  checkNodes=vueData.$refs.menuTree.getCheckedKeys();
    //取得当前半选中的节点
    let  checkHalfNodes=vueData.$refs.menuTree.getHalfCheckedKeys();

    vueData.form.menusId=checkHalfNodes.concat(checkNodes);

    saveData(vueData.form).then(resp => {
      // 关闭对话框
      vueData.dialogFormVisible=false
      // 重新查询数据
      loadData()

    })
  }
}

// 新增
function addForm() {
  vueData.title= "添加角色"
  // 显示对话框
  vueData.dialogFormVisible = true

  //树型节点互子关联
  vueData.checkStrictly=false

  vueData.$nextTick(() => {
    // 重置表单内容
    vueData.$refs.form.resetFields()

     //清空表单元素
     vueData.form={
        roleId: undefined,
        roleName: undefined,
        roleKey: undefined,
        status: 0,
        menusId: []
      }
    // 根据角色id 动态获取 权限
     vueData.$refs.menuTree.setCheckedKeys([])
  })
}

export default {
  init,
  loadData,
  editForm,
  saveForm,
  addForm
}
