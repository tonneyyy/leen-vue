// 引用数据
import {saveData,getTreeData} from '@/api/menu_ajax'

let vueData;

function init(_vueData){
  vueData = _vueData;
}
function addRootForm(){
  vueData.title = '添加菜单';
  vueData.menulable = '菜单名称';
  vueData.parentName = '主类目';
  vueData.menuTypeName='目录'
  vueData.dialogFormVisible = true;
  //清空数据
  vueData.form ={
          menuName:undefined,
          parentId:0,
          orderNum:undefined,
          path:undefined,
          menuType:"M",
          perms:undefined,
          visible:0,
          status:0
          }
}

function addForm(data){
  let type = data.menuType;
  vueData.title = '添加菜单';
  vueData.menulable = '菜单名称';
  let treeData = vueData.treeData;
  vueData.parentName = data.menuName;
  if(type === 'M'){
    vueData.menuTypeName='菜单'
    type = 'C';
  }else if(type === 'C'){
    vueData.menuTypeName='按钮'
    type = 'F';
    vueData.menulable = '按钮名称';
  }
  vueData.dialogFormVisible = true;
  //清空数据
  vueData.form ={
          menuName:undefined,
          parentId:data.menuId,
          orderNum:undefined,
          path:undefined,
          menuType:type,
          perms:undefined,
          isFrame:1,
          visible:0,
          status:0
          }
}
function editForm(data){
  vueData.title = '修改菜单';
  vueData.dialogFormVisible = true;
  //还原数据
  if(data.parentId === 0){
    vueData.parentName = '主类目'
  }else{
    let treeData = vueData.treeData;
    searchParentName(treeData,data.parentId);
  }

  if(data.menuType === 'M'){
    vueData.menuTypeName='目录'
  }else if(data.menuType === 'C'){
    vueData.menuTypeName='菜单'
  }else if(data.menuType === 'F'){
    vueData.menuTypeName='按钮'
    vueData.menulable = '按钮名称';
  }
  vueData.form = Object.assign({},vueData.form,data);
}

function searchParentName(one,parentId){
  for (let two of one) {
    if(two.menuId === parentId){
      vueData.parentName = two.menuName;
      return
    }
    if(two.hasOwnProperty('children')){
      searchParentName(two.children,parentId);
    }
  }
}

function saveForm(){
  //验证数据有效性
  let valid = true;
  if(valid){
    saveData(vueData.form).then((resp)=>{
      //关闭对话框
      vueData.dialogFormVisible = false
      //重新加载数据
      loadData()
    })
  }
}
//私有方法，用于加载数据
function loadData(){
  getTreeData().then(resp =>{
    if(resp.code == 1){
      vueData.treeData = resp.data
    }
  })
}

//打包，变为一个模块(类)
export default {
  init,
  loadData,
  addForm,
  addRootForm,
  editForm,
  saveForm
}
