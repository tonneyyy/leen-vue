// 引用 数据访问层
import { getData ,saveData,deleteData} from '@/api/courseteacher_ajax.js'
import { copyProperties } from '@/utils/BeanUtils'

let vueData;

function init(_vueData){
  vueData=_vueData;
}

// 加载数据
function loadData(){
  getData(vueData.searchForm).then( resp => {
    console.log(resp);
    vueData.responseVO = resp.data
  })
}

function editForm (data) {
  vueData.title= "修改课程老师"
  // 显示对话框
  vueData.dialogFormVisible = true
  // 还原数据
  vueData.form = data

}


// 保存操作
function saveForm() {
    saveData(vueData.form).then(resp => {
      // 关闭对话框
      vueData.dialogFormVisible=false
      // 重新查询数据
      loadData()

    })
  }

  // 删除操作
 function deleteForm(data) {
   vueData.$confirm('此操作将永久删除该数据, 是否继续?', '提示', {
     confirmButtonText: '确定',
     cancelButtonText: '取消',
     type: 'warning'
   }).then(() => {
     deleteData(data.id).then((resp) => {
       vueData.$message({
         type: 'success',
         message: '删除成功!'
       });
       loadData();
     });

   }).catch(() => {
     vueData.$message({
       type: 'info',
       message: '已取消删除'
     });
   });
 }

// 新增
function addForm() {
  vueData.title= "添加课程老师"
  // 显示对话框
  vueData.dialogFormVisible = true
  vueData.$nextTick(() => {
    // 重置表单内容
    vueData.$refs.form.resetFields()

     //清空表单元素
     vueData.form={
        id: undefined,
        name: undefined,
        portrait: undefined
      }

  })
}

export default {
  init,
  loadData,
  editForm,
  saveForm,
  addForm,
  deleteForm
}
